import { authRoutes } from './routes';
import Router from 'next/router';

export default (ctx) => {
	if (!ctx.isServer) {
		const path = ctx.pathname.replace('/', '');
		const isAuth = ctx.store.getState().auth.isAuth;
		const user = ctx.store.getState().auth.user;
		if (authRoutes.hasOwnProperty(path) && isAuth && user) {
			const userRol = user.rol.toLowerCase();
			const arr = authRoutes[path].roles
				.map((i) => {
					switch (i) {
						case 'root':
							if (userRol === 'root') {
								return true;
							} else {
								return false;
							}
						case 'admin':
							if (userRol === 'admin') {
								return true;
							} else {
								return false;
							}

						case 'user':
							if (userRol === 'user') {
								return true;
							} else {
								return false;
							}

						default:
							Router.push('/login');
							break;
					}
				})
				.filter((o) => o);

			if (arr.length === 0) {
				Router.push('/');
			}
		} else {
			Router.push('/login');
		}
	}
};
