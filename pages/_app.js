import { Provider } from 'react-redux';
import App from 'next/app';
import withRedux from 'next-redux-wrapper';
import { makeStore } from '../store';
import { PersistGate } from 'redux-persist/integration/react';

export default withRedux(makeStore, { debug: true })(
	class MyApp extends App {
		static async getInitialProps({ Component, ctx }) {
			return {
				pageProps : {
					...(Component.getInitialProps ? await Component.getInitialProps(ctx) : {})
				}
			};
		}

		render() {
			const { Component, pageProps, store } = this.props;
			return (
				<Provider store={store}>
					<PersistGate persistor={store.__persistor} loading={<div>Loading</div>}>
						<Component {...pageProps} />
					</PersistGate>
				</Provider>
			);
		}
	}
);
