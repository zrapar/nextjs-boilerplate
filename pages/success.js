import { connect } from 'react-redux';
import initialize from '../utils/initialize';
import checkAuth from '../utils/checkAuth';
import Layout from '../components/Layout';

const Success = () => <Layout title='Home'>SUCCESS</Layout>;

Success.getInitialProps = function(ctx) {
	initialize(ctx);
	checkAuth(ctx);
};

export default connect((state) => state)(Success);
