import { createStore, applyMiddleware } from 'redux';
import { composeWithDevTools } from 'redux-devtools-extension';
import thunk from 'redux-thunk';
import logger from 'redux-logger';
import reducer from './reducers';
import { persistStore, persistReducer } from 'redux-persist';
import storage from 'redux-persist/lib/storage'; // defaults to localStorage for web
const persistConfig = {
	key     : 'nextjs',
	storage
};
const persistedReducer = persistReducer(persistConfig, reducer);
const initialState = {};

// const storeVar = createStore(persistedReducer, initialState, composeWithDevTools(applyMiddleware(thunk)));
const storeVar = createStore(persistedReducer, initialState, composeWithDevTools(applyMiddleware(thunk)));
const persistorVar = persistStore(storeVar);

export const initStore = storeVar;
export const persistor = persistorVar;

const makeConfiguredStore = (reducer, initialState) => createStore(reducer, initialState, applyMiddleware(thunk));

export const makeStore = (initialState, { isServer, req, debug, storeKey }) => {
	if (isServer) {
		initialState = initialState;

		return makeConfiguredStore(reducer, initialState);
	} else {
		const store = storeVar;

		store.__persistor = persistorVar; // Nasty hack

		return store;
	}
};
