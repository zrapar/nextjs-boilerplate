import Router from 'next/router';
import axios from 'axios';
import { AuthTypes } from '../types';
import { API } from '../../config';
import { setCookie, removeCookie } from '../../utils/cookie';

// gets token from the api and stores it in the redux store and in cookie
const authenticate = ({ email, password }, type) => {
	if (type !== 'login') {
		throw new Error('Wrong API call!');
	}
	return (dispatch) => {
		axios
			.post(`${API}/${type}`, { email, password })
			.then((response) => {
				setCookie('token', response.data.data.token);
				dispatch({ type: AuthTypes.USER, payload: response.data.data });
				dispatch({ type: AuthTypes.AUTHENTICATE, payload: response.data.data.token });
				Router.push('/success');
			})
			.catch((err) => {
				console.log(err);
				switch (error.response.status) {
					case 422:
						alert(error.response.data.meta.message);
						break;
					case 401:
						alert(error.response.data.meta.message);
						break;
					case 500:
						alert('Interval server error! Try again!');
						break;
					default:
						alert(error.response.data.meta.message);
						break;
				}
			});
	};
};

// gets the token from the cookie and saves it in the store
const reauthenticate = (token) => {
	return (dispatch) => {
		dispatch({ type: AuthTypes.AUTHENTICATE, payload: token });
	};
};

// removing the token
const deauthenticate = () => {
	return (dispatch) => {
		removeCookie('token');
		Router.push('/');
		dispatch({ type: AuthTypes.DEAUTHENTICATE });
	};
};

const getUser = ({ token }, type) => {
	console.log(token);
	return (dispatch) => {
		axios
			.get(`${API}/${type}`, {
				headers : {
					Authorization : 'bearer ' + token
				}
			})
			.then((response) => {
				dispatch({ type: AuthTypes.USER, payload: response.data.data.user });
			})
			.catch((error) => {
				switch (error.response.status) {
					case 401:
						Router.push('/');
						break;
					case 422:
						alert(error.response.data.meta.message);
						break;
					case 500:
						alert('Interval server error! Try again!');
					case 503:
						alert(error.response.data.meta.message);
						Router.push('/');
						break;
					default:
						alert(error.response.data.meta.message);
						break;
				}
			});
	};
};

export default {
	authenticate,
	reauthenticate,
	deauthenticate,
	getUser
};
