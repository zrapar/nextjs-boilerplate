export const AuthTypes = {
	LOGIN_SUCCESS  : '@@Auth/Login Success ',
	LOGIN_FAIL     : '@@Auth/Login fail',
	LOGOUT         : '@@Auth/Logout',
	SET_USER       : '@@Auth/ Set User',
	AUTHENTICATE   : '@@Auth/ Authenticate',
	USER           : '@@Auth/ User',
	DEAUTHENTICATE : '@@Auth/ Deauthenticate'
};
