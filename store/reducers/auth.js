import { AuthTypes } from '../types';

const initialState = {
	token  : null,
	user   : null,
	isAuth : false
};

export default (state = initialState, action) => {
	const { type, payload } = action;
	switch (type) {
		case AuthTypes.AUTHENTICATE:
			return { ...state, token: payload, isAuth: true };
		case AuthTypes.USER:
			return { ...state, user: payload };
		case AuthTypes.DEAUTHENTICATE:
			return initialState;
		default:
			return state;
	}
};
